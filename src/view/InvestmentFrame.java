package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   private static final double DEFAULT_RATE = 5;
   
   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   
   public InvestmentFrame(ActionListener listener)
   {  
      // Use instance variables for components 
      resultLabel = new JLabel("balance: ");
	  createTextField();
	  createButton(listener);
	  createPanel();
      // Use helper methods 

   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton(ActionListener listener)
   {
      button = new JButton("Add Interest");
      button.addActionListener(listener);
   }

   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }
   public String getTextRateField(){
	   return rateField.getText();
   }
   
   public void setResultLable(String str){
	   resultLabel.setText("balance: "+str);
   }
}
