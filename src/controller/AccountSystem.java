package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.BankAccount;
import view.InvestmentFrame;

public class AccountSystem {
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private static final double INITIAL_BALANCE = 1000;
	// controller �����  bankaccount ��� InvestmentFrame �� attribute ����  controller ��͵�ǨѴ��� �����¡  model view �ҷӧҹ ��� local �з������������ҨФǺ����Ѵ�������
	// ��䢧���
	private InvestmentFrame frame;
	private BankAccount account ;
    ActionListener listener ;


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AccountSystem();
	}

	public AccountSystem() {
		listener = new AddInterestListener();
		account = new BankAccount(INITIAL_BALANCE);
		frame = new InvestmentFrame(listener);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		//frame.pack();
	    frame.setVisible(true);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTestCase();
	}

	public void setTestCase() {

	}
	class AddInterestListener implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {
           double rate = Double.parseDouble(frame.getTextRateField());
           double interest = account.getBalance() * rate / 100;
           account.deposit(interest);
           frame.setResultLable(account.getBalance()+"");
        }            
     }
     

 

}